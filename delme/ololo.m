//
//  ololo.m
//  delme
//
//  Created by Dmitriy Gorbel on 6/4/14.
//  Copyright (c) 2014 tundramobile. All rights reserved.
//

#import "ololo.h"
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

@implementation ololo

- (void)test
{
    NSMutableArray* array = [NSMutableArray array];
    for(int i = 0; 100000 > i; i++) {
        [array addObject:@(arc4random() % 10000)];
    }
    NSLog(@"Start Obj-C ====================");
     [array sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 integerValue] < [obj2 integerValue])
            return NSOrderedAscending;
        else if ([obj1 integerValue] > [obj2 integerValue])
            return NSOrderedDescending;
        else
            return NSOrderedSame;
    }];
    NSLog(@"End Obj-C ====================");

}

@end
